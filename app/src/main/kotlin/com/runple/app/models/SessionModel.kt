package com.runple.app.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import org.joda.time.DateTime

interface Session : Parcelable {
    var accessToken: String?
    var refreshToken: String?
    var expireDate: DateTime?

    fun newSession(accessToken: String?,
                   refreshToken: String?,
                   expireDate: DateTime?) {
        this.accessToken = accessToken
        this.refreshToken = refreshToken
        this.expireDate = expireDate
    }

    fun clear() {
        this.accessToken = null
        this.refreshToken = null
        this.expireDate = null
    }
}

@Parcelize
data class SessionModel(override var accessToken: String? = null,
                        override var refreshToken: String? = null,
                        override var expireDate: DateTime? = null) : Session
