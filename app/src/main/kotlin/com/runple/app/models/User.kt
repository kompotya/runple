package com.runple.app.models

import kotlinx.android.parcel.Parcelize


interface User : Model<Long> {
    val email: String?
    val phone: String?
}

@Parcelize
data class UserModel(override var id: Long? = null,
                     override var email: String? = null,
                     override var phone: String? = null) : User