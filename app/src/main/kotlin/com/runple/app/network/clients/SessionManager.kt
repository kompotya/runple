package com.runple.app.network.clients

import com.runple.app.network.api.beans.SessionBean


interface SessionManager {
    var token: String?
    var refreshToken: String?
    val isSessionActive: Boolean
    fun saveSession(session: SessionBean?)
    fun clearSession(isLogout: Boolean = false)
}