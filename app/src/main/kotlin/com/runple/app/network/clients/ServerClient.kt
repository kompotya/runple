package com.runple.app.network.clients

import com.ihsanbal.logging.Level
import com.ihsanbal.logging.LoggingInterceptor
import com.runple.app.BuildConfig
import com.runple.app.network.NetworkModule.mapper
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import okhttp3.internal.platform.Platform
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory
import java.util.concurrent.TimeUnit

class ServerClient(private val sessionManager: SessionManager) {

    companion object {
        private const val HEADER_AUTHORIZATION = "Authorization"
        private const val AUTHORIZATION_TOKEN_TYPE = "Bearer"

        private const val SERVER_TIMEOUT_IN_SECONDS = 30L
    }

    val retrofit: Retrofit = Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(JacksonConverterFactory.create(mapper))
            .baseUrl(createApiEndpoint())
            .client(createHttpClient())
            .build()

    private fun log() = LoggingInterceptor.Builder()
            .loggable(BuildConfig.DEBUG)
            .setLevel(Level.BASIC)
            .log(Platform.INFO)
            .request("Request>>>>")
            .response("Response<<<<")
            .build()

    private fun createHttpClient(): OkHttpClient = OkHttpClient.Builder()
            .connectTimeout(SERVER_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)
            .readTimeout(SERVER_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)
            .writeTimeout(SERVER_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)
            .addInterceptor(object : Interceptor {
                override fun intercept(chain: Interceptor.Chain): Response {
                    val original = chain.request()
                    val requestBuilder = original.newBuilder()
                    requestBuilder.method(original.method, original.body)
                    return chain.proceed(addHeader(requestBuilder))
                }
            })
            .authenticator(SessionAuthenticator(sessionManager))
            .apply {
                if (BuildConfig.DEBUG) addInterceptor(log())
            }.build()

    private fun addHeader(requestBuilder: Request.Builder): Request = requestBuilder
            .removeHeader(HEADER_AUTHORIZATION)
            .apply {
                if (sessionManager.isSessionActive) {
                    addHeader(HEADER_AUTHORIZATION, "$AUTHORIZATION_TOKEN_TYPE ${sessionManager.token}")
                }
            }
            .build()

    private fun createApiEndpoint() = "${BuildConfig.ENDPOINT}/api/"
}
