package com.runple.app.network.api.converters

import com.runple.app.models.Session
import com.runple.app.models.SessionModel
import com.runple.app.models.converters.BaseConverter
import com.runple.app.network.api.beans.SessionBean

interface SessionBeanConverter

class SessionBeanConverterImpl : BaseConverter<SessionBean, Session>(), SessionBeanConverter {

    override fun processConvertInToOut(inObject: SessionBean?) = inObject?.run {
        SessionModel(accessToken, refreshToken, expiresAt)
    }

    override fun processConvertOutToIn(outObject: Session?) = outObject?.run {
        SessionBean(accessToken, refreshToken, expireDate)
    }
}
