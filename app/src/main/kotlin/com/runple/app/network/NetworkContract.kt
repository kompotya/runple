package com.runple.app.network

internal const val LOCALHOST = "http://localhost:"

internal const val UNAUTHORIZED = 401
internal const val BAD_TOKEN = 400

const val V1 = "v1"