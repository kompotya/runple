package com.runple.app.network

import com.runple.app.network.api.retrofitApi.AccountApi
import com.runple.app.network.api.retrofitApi.SessionApi
import com.runple.app.network.modules.AccountModule
import com.runple.app.network.modules.AccountModuleImpl
import com.runple.app.network.modules.SessionModule
import com.runple.app.network.modules.SessionModuleImpl


object NetworkModuleInjector {
    private var accountModule: AccountModule? = null
    private var sessionModule: SessionModule? = null

    fun getAccountModule(): AccountModule =
            accountModule
                    ?: AccountModuleImpl(NetworkModule.client.retrofit.create(AccountApi::class.java))
                            .apply { accountModule = this }

    internal fun getSessionModule(): SessionModule =
            sessionModule
                    ?: SessionModuleImpl(NetworkModule.client.retrofit.create(SessionApi::class.java))
                            .apply { sessionModule = this }
}