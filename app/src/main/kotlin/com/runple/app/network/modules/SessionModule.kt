package com.runple.app.network.modules

import com.runple.app.network.api.beans.SessionBean
import com.runple.app.network.api.requests.RefreshTokenRequest
import com.runple.app.network.api.retrofitApi.SessionApi
import io.reactivex.Single


interface SessionModule {

    fun refreshToken(refreshToken: String?): Single<SessionBean>
}

internal class SessionModuleImpl(sessionApi: SessionApi) :BaseRxModule<SessionApi>(sessionApi), SessionModule {

    override fun refreshToken(refreshToken: String?): Single<SessionBean> =
            api.refreshToken(RefreshTokenRequest(refreshToken))
                    .onErrorResumeNext(NetworkErrorUtils.rxParseSingleError())
                    .map { it.data }
}