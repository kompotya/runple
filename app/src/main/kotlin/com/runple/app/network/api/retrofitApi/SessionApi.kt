package com.runple.app.network.api.retrofitApi

import com.runple.app.network.V1
import com.runple.app.network.api.beans.Response
import com.runple.app.network.api.beans.SessionBean
import com.runple.app.network.api.requests.RefreshTokenRequest
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.POST


interface SessionApi {
    @POST("$V1/account/refreshToken")
    fun refreshToken(@Body request: RefreshTokenRequest): Single<Response<SessionBean>>
}