package com.runple.app.network.clients

import com.cleveroad.bootstrap.kotlin_core.network.ApiException
import com.cleveroad.bootstrap.kotlin_core.utils.ioToMainSingle
import com.runple.app.network.BAD_TOKEN
import com.runple.app.network.NetworkModuleInjector
import com.runple.app.network.UNAUTHORIZED
import io.reactivex.Single
import io.reactivex.SingleTransformer
import okhttp3.Authenticator
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route
import retrofit2.HttpException


class SessionAuthenticator(private val sessionManager: SessionManager): Authenticator {

    companion object {
        private const val HEADER_AUTHORIZATION = "Authorization"
        private const val AUTHORIZATION_TOKEN_TYPE = "Bearer"

        private const val MAX_REPEAT_TOKEN_REFRESH_COUNT = 2
    }

    private val sessionModule by lazy { NetworkModuleInjector.getSessionModule() }

    override fun authenticate(route: Route?, response: Response): Request? {
        if (sessionManager.token.isNullOrEmpty()
                || responseCount(response) >= MAX_REPEAT_TOKEN_REFRESH_COUNT) {
            return null
        }
        val session = sessionModule.refreshToken(sessionManager.refreshToken)
                .compose(unauthorizedHandler())
                .compose(ioToMainSingle())
                .blockingGet()
        sessionManager.saveSession(session)
        return addHeader(response.request.newBuilder())
    }

    private fun responseCount(response: Response): Int {
        var result = 0
        do {
            result++
        } while (response.priorResponse != null)

        return result
    }

    private fun <T> unauthorizedHandler() = SingleTransformer<T, T> {
        it.onErrorResumeNext { throwable: Throwable ->
            return@onErrorResumeNext when {
                throwable is ApiException && throwable.statusCode == BAD_TOKEN -> doLogoutSingle()
                throwable !is HttpException || throwable.code() != UNAUTHORIZED -> Single.error<T>(throwable)
                else -> doLogoutSingle()
            }
        }
    }

    private fun addHeader(requestBuilder: Request.Builder): Request = requestBuilder
            .removeHeader(HEADER_AUTHORIZATION)
            .apply {
                if (sessionManager.isSessionActive) {
                    addHeader(HEADER_AUTHORIZATION, "$AUTHORIZATION_TOKEN_TYPE ${sessionManager.token}")
                }
            }
            .build()

    private fun <T> doLogoutSingle(): Single<T> {
        sessionManager.clearSession(true)
        return Single.error<T>(IllegalAccessException("Wrong credentials"))
    }
}