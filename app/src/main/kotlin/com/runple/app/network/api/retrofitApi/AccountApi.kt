package com.runple.app.network.api.retrofitApi

import com.runple.app.network.V1
import com.runple.app.network.api.beans.Response
import com.runple.app.network.api.beans.UserSessionBean
import com.runple.app.network.api.requests.LoginRequest
import com.runple.app.network.api.requests.RegisterRequest
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.POST

interface AccountApi {
    // TODO Starter: change links for methods
    @POST("$V1/account")
    fun register(@Body request: RegisterRequest): Single<Response<UserSessionBean>>

    @POST("$V1/account/login")
    fun login(@Body request: LoginRequest): Single<Response<UserSessionBean>>

    @POST("$V1/account/logout")
    fun logout(): Single<Response<Unit>>

    @POST("$V1/account/restorePassword")
    fun restorePassword(): Single<Response<Unit>>

    @POST("$V1/account/confirmNewPassword")
    fun confirmPassword(newPassword: String): Single<Response<Unit>>
}
