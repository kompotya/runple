package com.runple.app.network.exceptions

import com.runple.app.R
import com.runple.app.extensions.appString


class SessionForbiddenException : Exception() {

    companion object {
        private val ERROR_MESSAGE = appString(R.string.session_forbidden_error)
    }

    override val message: String = ERROR_MESSAGE
}