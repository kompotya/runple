package com.runple.app.network.api.converters

import com.runple.app.models.User
import com.runple.app.models.UserModel
import com.runple.app.models.converters.BaseConverter
import com.runple.app.network.api.beans.UserBean


class UserBeanConverter : BaseConverter<UserBean, User>() {

    override fun processConvertInToOut(inObject: UserBean?) = inObject?.run {
        UserModel(id,
                email,
                phone)
    }

    override fun processConvertOutToIn(outObject: User?) = outObject?.run {
        UserBean(id,
                email,
                phone)
    }
}
