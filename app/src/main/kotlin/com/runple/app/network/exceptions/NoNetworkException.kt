package com.runple.app.network.exceptions

import com.runple.app.R
import com.runple.app.extensions.appString

class NoNetworkException : Exception() {

    companion object {
        private val ERROR_MESSAGE = appString(R.string.no_internet_connection_error)
    }

    override val message: String = ERROR_MESSAGE
}
