package com.runple.app.network.api.beans

import com.fasterxml.jackson.annotation.JsonProperty


data class UserBean(@JsonProperty("id")
                    val id: Long?,
                    @JsonProperty("email")
                    val email: String?,
                    @JsonProperty("phone")
                    val phone: String?)