package com.runple.app.network.modules

import com.runple.app.network.api.beans.UserSessionBean
import com.runple.app.network.api.requests.LoginRequest
import com.runple.app.network.api.requests.RegisterRequest
import com.runple.app.network.api.retrofitApi.AccountApi
import com.runple.app.preferences.PreferenceModule
import io.reactivex.Single

interface AccountModule {

    fun register(request: RegisterRequest): Single<UserSessionBean>

    fun login(email: String, password: String): Single<UserSessionBean>

    fun logout(): Single<Unit>

    fun restorePassword(): Single<Unit>

    fun confirmNewPassword(newPassword: String): Single<Unit>
}

class AccountModuleImpl(api: AccountApi) :
        BaseRxModule<AccountApi>(api), AccountModule {

    private val sessionInstance = PreferenceModule.session

    override fun register(request: RegisterRequest): Single<UserSessionBean> =
            api.register(request)
                    .onErrorResumeNext(NetworkErrorUtils.rxParseSingleError())
                    .map { it.data }

    override fun login(email: String, password: String): Single<UserSessionBean> =
            api.login(LoginRequest(email, password))
                    .onErrorResumeNext(NetworkErrorUtils.rxParseSingleError())
                    .map { it.data }
                    .doOnSuccess {
                        it.apply { sessionInstance.saveSession(session) }
                    }

    override fun logout(): Single<Unit> =
            api.logout()
                    .onErrorResumeNext(NetworkErrorUtils.rxParseSingleError())
                    .map { it.data }

    override fun restorePassword() =
            api.restorePassword()
                    .onErrorResumeNext(NetworkErrorUtils.rxParseSingleError())
                    .map { it.data }

    override fun confirmNewPassword(newPassword: String) =
            api.confirmPassword(newPassword)
                    .onErrorResumeNext(NetworkErrorUtils.rxParseSingleError())
                    .map { it.data }
}
