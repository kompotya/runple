package com.runple.app.utils.simple

import android.animation.Animator

open class SimpleAnimatorListener(private val animationEnd: () -> Unit = {})
    : Animator.AnimatorListener {

    override fun onAnimationRepeat(animation: Animator?) = Unit

    override fun onAnimationEnd(animation: Animator?) {
        animationEnd.invoke()
    }

    override fun onAnimationCancel(animation: Animator?) = Unit

    override fun onAnimationStart(animation: Animator?) = Unit

}