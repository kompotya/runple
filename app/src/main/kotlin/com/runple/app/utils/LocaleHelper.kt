package com.runple.app.utils

import android.content.Context
import android.os.Build
import com.runple.app.preferences.PreferenceModule
import java.util.*
import android.os.LocaleList


class LocaleContextWrapper(base: Context) : android.content.ContextWrapper(base) {

    companion object {

        fun wrap(newContext: Context, newLocale: Locale): LocaleContextWrapper {
            var context = newContext
            val res = context.resources
            val configuration = res.configuration

            context = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                configuration.setLocale(newLocale)

                val localeList = LocaleList(newLocale)
                LocaleList.setDefault(localeList)
                configuration.setLocales(localeList)

                context.createConfigurationContext(configuration)

            } else  {
                configuration.setLocale(newLocale)
                context.createConfigurationContext(configuration)
            }

            return LocaleContextWrapper(context)
        }

        fun setNewLocale(context: Context, locale: Locale) = run {
            persistLanguagePreference(locale.language)
            wrap(context, locale)
        }

        private fun persistLanguagePreference(language: String) {
            PreferenceModule.lang = language
        }
    }
}