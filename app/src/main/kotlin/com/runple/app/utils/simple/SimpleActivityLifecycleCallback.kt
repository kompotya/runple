package com.runple.app.utils.simple

import android.app.Activity
import android.app.Application
import android.os.Bundle

open class SimpleActivityLifecycleCallback(
        private val onActivityCreated: (activity: Activity) -> Unit = {}
) : Application.ActivityLifecycleCallbacks {
    override fun onActivityPaused(activity: Activity) {

    }

    override fun onActivityStarted(activity: Activity) {
    }

    override fun onActivityDestroyed(activity: Activity) {
    }

    override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {
    }

    override fun onActivityStopped(activity: Activity) {
    }

    override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
        onActivityCreated(activity)
    }

    override fun onActivityResumed(activity: Activity) {
    }

}