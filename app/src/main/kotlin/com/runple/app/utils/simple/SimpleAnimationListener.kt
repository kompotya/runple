package com.runple.app.utils.simple

import android.view.animation.Animation

open class SimpleAnimationListener(private val animationEnd: () -> Unit = {})
    : Animation.AnimationListener {

    override fun onAnimationRepeat(animation: Animation?) = Unit

    override fun onAnimationEnd(animation: Animation?) = animationEnd()

    override fun onAnimationStart(animation: Animation?) = Unit
}