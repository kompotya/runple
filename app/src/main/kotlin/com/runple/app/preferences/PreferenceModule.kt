package com.runple.app.preferences

import com.runple.app.RunApp


object PreferenceModule {

    val session: SessionModule by lazy { SessionModuleImpl(RunApp.prefs) }

    var lang by StringPD()

    fun clearPrefs() {
        session.clearSession(false)
    }
}