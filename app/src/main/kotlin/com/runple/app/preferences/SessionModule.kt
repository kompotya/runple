package com.runple.app.preferences

import android.content.SharedPreferences
import com.runple.app.RunApp
import com.runple.app.network.api.beans.SessionBean
import com.runple.app.network.clients.SessionManager


interface SessionModule : SessionManager {
    override val isSessionActive: Boolean
    override var token: String?
    override var refreshToken: String?
    var isFirstSession: Boolean
}

class SessionModuleImpl(pref: SharedPreferences) : SessionModule {

    override var token by StringPD(pref = pref)
    override var refreshToken by StringPD(pref = pref)
    override val isSessionActive: Boolean
        get() = token != null

    override var isFirstSession by BooleanPD(pref = pref)

    override fun saveSession(session: SessionBean?) {
        this.token = session?.accessToken
        this.refreshToken = session?.refreshToken
    }

    override fun clearSession(isLogout: Boolean) {
        if (isLogout) {
            RunApp.instance.onLogout()
        } else {
            token = null
            refreshToken = null
        }
    }
}

