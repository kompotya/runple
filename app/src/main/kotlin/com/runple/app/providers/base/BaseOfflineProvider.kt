package com.runple.app.providers.base

import com.runple.app.database.repositories.Repository
import com.runple.app.models.Model
import com.runple.app.models.converters.Converter


abstract class BaseOfflineProvider<T : Comparable<T>,
        DBModel,
        M : Model<T>,
        Repo : Repository<DBModel>> : Provider<T, M> {

    val repository: Repo = this.initRepository()

    abstract val databaseConverter: Converter<DBModel, M>

    protected abstract fun initRepository(): Repo
}