package com.runple.app.providers.base

import com.runple.app.models.Model


interface Provider<AppModelType : Comparable<AppModelType>,
        AppModel : Model<AppModelType>>