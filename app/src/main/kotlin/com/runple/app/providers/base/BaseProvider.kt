package com.runple.app.providers.base

import com.runple.app.database.repositories.Repository
import com.runple.app.models.Model


abstract class BaseProvider<NetworkModel,
        DBModel,
        AppModelType : Comparable<AppModelType>,
        AppModel : Model<AppModelType>,
        NetworkModule,
        Repo : Repository<DBModel>>
    : BaseOnlineProvider<NetworkModel, AppModelType, AppModel, NetworkModule>() {

    val repository: Repo = this.initRepository()

    protected abstract fun initRepository(): Repo
}