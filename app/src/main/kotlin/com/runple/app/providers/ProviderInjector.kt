package com.runple.app.providers


object ProviderInjector {

    private var accountProvider: AccountProvider? = null

    fun getAccountProvider(): AccountProvider =
            accountProvider ?: AccountProviderImpl().apply {
                accountProvider = this
            }
}
