package com.runple.app.providers

import com.runple.app.database.DatabaseModuleInjector
import com.runple.app.database.repositories.UserRepositoryImpl
import com.runple.app.database.tables.UserDb
import com.runple.app.models.User
import com.runple.app.models.UserModel
import com.runple.app.network.NetworkModuleInjector
import com.runple.app.network.api.beans.UserBean
import com.runple.app.network.api.converters.UserBeanConverter
import com.runple.app.network.modules.AccountModule
import com.runple.app.providers.base.BaseProvider
import io.reactivex.Single
import io.reactivex.SingleTransformer


interface AccountProvider {

    fun register(fName: String,
                 lName: String,
                 email: String,
                 password: String,
                 confirmPassword: String): Single<Unit>

    fun login(email: String, password: String): Single<Unit>

    fun logout(): Single<Unit>

    fun restorePassword(email: String): Single<Unit>

    fun confirmPassword(newPassword: String): Single<Unit>
}


class AccountProviderImpl : BaseProvider<UserBean, UserDb, Long, User, AccountModule, UserRepositoryImpl>(), AccountProvider {

    override val networkConverter = UserBeanConverter()

    override fun initNetworkModule() = NetworkModuleInjector.getAccountModule()

    override fun initRepository() = DatabaseModuleInjector.getProfileRepository()

    override fun initNewModel() = UserModel()

    override fun login(email: String, password: String) = Single.just(Unit)

    override fun logout(): Single<Unit> =
            Single.just(Unit)

    override fun register(fName: String,
                          lName: String,
                          email: String,
                          password: String,
                          confirmPassword: String) = Single.just(Unit)

    override fun confirmPassword(newPassword: String) = Single.just(Unit)

    override fun restorePassword(email: String) = Single.just(Unit)

    private fun saveUser(): SingleTransformer<User, User> = SingleTransformer { IN ->
        IN.flatMap { user ->
            repository.saveUser(user)
                    .map { user }
        }
    }
}