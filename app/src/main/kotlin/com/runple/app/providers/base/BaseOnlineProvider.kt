package com.runple.app.providers.base

import com.runple.app.models.Model
import com.runple.app.models.converters.Converter


abstract class BaseOnlineProvider<NetworkModel,
        AppModelType : Comparable<AppModelType>,
        AppModel : Model<AppModelType>,
        NetworkModule> : Provider<AppModelType, AppModel> {

    val networkModule: NetworkModule = this.initNetworkModule()

    abstract val networkConverter: Converter<NetworkModel, AppModel>

    protected abstract fun initNetworkModule(): NetworkModule

    abstract fun initNewModel(): AppModel
}