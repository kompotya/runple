package com.runple.app.extensions

import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.File

private const val FORM_DATA_NAME = "file"
private const val FORM_DATA_TYPE = "multipart/form-data"

fun File.getMultipart(
        formDataName: String = FORM_DATA_NAME,
        formDataType: String = FORM_DATA_TYPE
): MultipartBody.Part {
    return MultipartBody.Part.createFormData(
            formDataName,
            name,
            asRequestBody(formDataType.toMediaTypeOrNull())
    )
}