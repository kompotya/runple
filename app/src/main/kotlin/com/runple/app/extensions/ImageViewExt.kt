package com.runple.app.extensions

import android.graphics.Bitmap
import android.widget.ImageView
import androidx.annotation.DrawableRes
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions

fun ImageView.loadImage(path: String) =
        Glide.with(context)
                .load(path)
                .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL))
                .into(this)

fun ImageView.loadImageCenterCrop(imageUri: String) =
        Glide.with(context)
                .load(imageUri)
                .centerCrop()
                .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL))
                .into(this)

fun ImageView.loadThumbCenterCrop(imageUri: String, sizeMultiplier: Float = 0.25f) =
        Glide.with(context)
                .load(imageUri)
                .centerCrop()
                .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL))
                .thumbnail(sizeMultiplier)
                .into(this)

fun ImageView.loadImageCenterCrop(bitmap: Bitmap) =
        Glide.with(context)
                .load(bitmap)
                .centerCrop()
                .into(this)

fun ImageView.loadImageCenterCrop(imageUri: String, @DrawableRes placeholder: Int) =
        Glide.with(context)
                .load(imageUri)
                .centerCrop()
                .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL))
                .apply(RequestOptions().error(placeholder).placeholder(placeholder))
                .into(this)

/**
 * Load image from uri to ImageView or set placeholder if load fails
 * load image without crop and with rounding it
 *
 * @param imageUri [String]
 */
fun ImageView.loadCircularImage(imageUri: String?) {
    Glide.with(context)
            .load(imageUri)
            .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL))
            .apply(RequestOptions().circleCrop())
            .into(this)
}

/**
 * Load image from uri to ImageView or set placeholder if load fails
 * load image without crop and with rounding it
 *
 * @param imageUri [String]
 */
fun ImageView.loadCircularImage(imageUri: String?, @DrawableRes placeholder: Int) =
        Glide.with(context)
                .load(imageUri)
                .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL))
                .apply(RequestOptions().error(placeholder).placeholder(placeholder).circleCrop())
                .into(this)


fun ImageView.loadThumbnail(imageUri: String?, sizeMultiplier: Float = 0.25f) =
        Glide.with(context)
                .load(imageUri)
                .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL))
                .thumbnail(sizeMultiplier)
                .into(this)

fun ImageView.loadCircularThumbnail(imageUri: String?, @DrawableRes placeholder: Int, sizeMultiplier: Float = 0.25f) =
        Glide.with(context)
                .load(imageUri)
                .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL))
                .apply(RequestOptions().error(placeholder).placeholder(placeholder).circleCrop())
                .thumbnail(sizeMultiplier)
                .into(this)