package com.runple.app.extensions

import android.content.Context
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.AbsoluteSizeSpan
import android.text.style.ImageSpan
import android.text.style.StyleSpan
import android.text.style.TextAppearanceSpan
import androidx.annotation.StyleRes

fun CharSequence.spannableSize(textSize: Int, isDip: Boolean,
                               startIndex: Int = 0,
                               endIndex: Int = length): SpannableStringBuilder = SpannableStringBuilder(this).apply {
    setSpan(AbsoluteSizeSpan(textSize, isDip), startIndex, endIndex, Spanned.SPAN_INCLUSIVE_EXCLUSIVE)
}

fun CharSequence.spannableDrawable(drawable: Drawable,
                                   startIndex: Int = 0): SpannableStringBuilder = SpannableStringBuilder(this).apply {
    drawable.setBounds(0, 0, drawable.intrinsicWidth, drawable.intrinsicHeight)
    setSpan(ImageSpan(drawable, ImageSpan.ALIGN_BASELINE), startIndex, startIndex + 1, Spanned.SPAN_INCLUSIVE_EXCLUSIVE)
}

fun CharSequence.spannableStyle(@StyleRes styleRes: Int,
                                context: Context,
                                startIndex: Int = 0,
                                endIndex: Int = length): SpannableStringBuilder = SpannableStringBuilder(this).apply {
    setSpan(TextAppearanceSpan(context, styleRes), startIndex, endIndex, Spanned.SPAN_INCLUSIVE_EXCLUSIVE)
}

fun CharSequence.spannableBold(startIndex: Int = 0,
                               endIndex: Int = length): SpannableStringBuilder = SpannableStringBuilder(this).apply {
    setSpan(StyleSpan(Typeface.BOLD), startIndex, endIndex, Spanned.SPAN_INCLUSIVE_EXCLUSIVE)
}