package com.runple.app.extensions

import android.content.Intent

inline fun <reified M> Intent.getExtraModel(extraName: String, unBoxedExtra: (M) -> Unit) =
        takeIf { it.hasExtra(extraName) }?.apply {
            (extras?.get(extraName) as? M)?.let { unBoxedExtra(it) }
        }

inline fun <reified M> Intent.getExtraModel(extraName: String,
                                            unBoxedExtra: (M) -> Unit,
                                            noExtra: () -> Unit = {}) =
        takeIf { it.hasExtra(extraName) }?.apply {
            (extras?.get(extraName) as? M)?.let { unBoxedExtra(it) } ?: noExtra()
        } ?: noExtra()