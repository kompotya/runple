package com.runple.app.extensions

import android.text.*
import android.text.Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.View
import android.widget.TextView
import com.cleveroad.bootstrap.kotlin_ext.hide
import com.cleveroad.bootstrap.kotlin_ext.show
import com.runple.app.R
import com.runple.app.utils.EMPTY_STRING

/**
 * The method attaches objects [ClickableSpan] to parts of the text to handle the clicks.
 *
 * @param initText some text [String] which need add to start of line
 * @param separateText some text [String] which will be used for separating clickable text
 * @param updateDrawState use for set extra parameter to clickable text
 * @param onClick handle click
 *
 */
fun <T> TextView.addClickableText(initText: String,
                                  separateText: String,
                                  clickableText: List<Pair<T, String>>,
                                  onClick: (T) -> Unit) {
    text = clickableText.foldIndexed(TextUtils.concat(initText)) { pos, acc, item ->
        val clickableSpan = object : ClickableSpan() {
            override fun onClick(textView: View) {
                onClick(item.first)
            }

            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.run {
                    color = appColor(R.color.black_24)
                    isUnderlineText = false
                    bgColor = appColor(R.color.bg_white)
                }
            }
        }
        SpannableString(item.second).run {
            setSpan(clickableSpan, 0, this.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
            if (pos == 0) TextUtils.concat(acc, this) else TextUtils.concat(acc, separateText, this)
        }
    }
    movementMethod = LinkMovementMethod.getInstance()
}

/**
 * Hide [TextView] if @param[string] is empty
 *
 * @param string [String] text which must shown in TextView
 * @param isGone [Boolean] if true TextView will be gone, else invisible
 */
fun TextView.hideIfEmpty(string: String?, isGone: Boolean = true) =
        string.takeUnless { it.isNullOrBlank() }
                ?.let {
                    show()
                    text = it
                }
                ?: run {
                    text = EMPTY_STRING
                    hide(isGone)
                }

/**
 * Attach the clickable span [ClickableSpan] to the range [start]…[end] of the [spannable].
 *
 * @param spannable [Spannable] current text.
 * @param textPart [String] clickable text part.
 * @param start [Int] span start.
 * @param end [Int] span end.
 *
 * @return [Unit]
 */
private fun CharSequence.setClickableSpan(textPart: String, start: Int, end: Int,
                                          callback: (String) -> Unit,
                                          isUnderline: Boolean = false) =
        SpannableStringBuilder(this).apply {
            setSpan(object : ClickableSpan() {
                override fun onClick(view: View) {
                    callback.invoke(textPart)
                }

                override fun updateDrawState(ds: TextPaint) {
                    ds.run {
                        isUnderlineText = isUnderline
                    }
                }
            }, start, end, SPAN_EXCLUSIVE_EXCLUSIVE)
        }