package com.runple.app.extensions

import android.widget.EditText
import com.runple.app.R
import com.runple.app.utils.EMPTY_STRING
import com.runple.app.utils.simple.SimpleTextWatcher
import com.cleveroad.bootstrap.kotlin_ext.addFocusChangedListener
import com.cleveroad.bootstrap.kotlin_ext.disableEditable
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.FlowableEmitter
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

fun EditText.text() = text.toString()

fun EditText.addFocusChangedValidation(callback: () -> Unit) {
    addFocusChangedListener { focus ->
        if (!focus) callback()
    }
}

fun EditText.setNonEditableText(text: String?) =
        apply {
            setText(text ?: EMPTY_STRING)
            disableEditable()
        }

/**
 * Wrap TextChangedListener with Flowable
 * For example, when you need to implement search view
 * Flowable will emit new characters
 */
fun EditText.getTextChangedFlowable() =
        Flowable.create({ emitter: FlowableEmitter<String> ->
            var watcher: SimpleTextWatcher?
            addTextChangedListener(object : SimpleTextWatcher() {
                override fun onTextChanged(sequence: CharSequence?, start: Int, before: Int, count: Int) {
                    super.onTextChanged(sequence, start, before, count)
                    sequence?.let { emitter.onNext(it.toString()) }
                }
            }.apply { watcher = this })
            emitter.setDisposable(object : Disposable {
                override fun isDisposed() = true

                override fun dispose() {
                    watcher?.let { removeTextChangedListener(it) }
                    watcher = null
                }
            })
        }, BackpressureStrategy.LATEST)
                .observeOn(Schedulers.io())
