package com.runple.app.extensions

import android.content.Context
import androidx.annotation.ArrayRes
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import com.runple.app.RunApp

fun appString(@StringRes resId: Int) = RunApp.instance.getString(resId)

fun appInteger(intRes: Int) = RunApp.instance.resources.getInteger(intRes)

fun appString(@StringRes stringId: Int, vararg formatArgs: Any) =
        RunApp.instance.getString(stringId, *formatArgs)

fun appStringArray(@ArrayRes id: Int) = RunApp.instance.resources.getStringArray(id)

fun appDrawable(@DrawableRes resId: Int) = ContextCompat.getDrawable(RunApp.instance, resId)

fun appColor(@ColorRes colorRes: Int,
             context: Context = RunApp.instance) = ContextCompat.getColor(context, colorRes)
