package com.runple.app.extensions

import android.os.SystemClock
import android.view.View
import android.view.View.MeasureSpec.makeMeasureSpec
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.animation.Transformation
import androidx.annotation.AnimRes
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import com.cleveroad.bootstrap.kotlin_ext.hide
import com.cleveroad.bootstrap.kotlin_ext.show
import com.runple.app.R
import com.runple.app.utils.simple.SimpleAnimationListener

private const val DEFAULT_CLICK_DEBOUNCE_TIME = 500L
private const val DEFAULT_ANIMATION_DURATION = 300L

fun View.applyLayoutParams(block: ViewGroup.MarginLayoutParams.() -> Unit) =
        (layoutParams as? ViewGroup.MarginLayoutParams)
                ?.let { layoutParams = it.apply { block(this) } }

fun View.clickWithDebounce(debounceTime: Long = DEFAULT_CLICK_DEBOUNCE_TIME, action: () -> Unit) {
    setOnClickListener(object : View.OnClickListener {
        private var lastClickTime = 0L
        override fun onClick(v: View) {
            SystemClock.elapsedRealtime().takeIf { it - lastClickTime > debounceTime }
                    ?.run {
                        action()
                        lastClickTime = this
                    }
        }
    })
}

fun View.OnClickListener.setClickListenerWithDebounce(vararg views: View, debounceTime: Long = DEFAULT_CLICK_DEBOUNCE_TIME) {
    val clickListener = object : View.OnClickListener {
        private var lastClickTime = 0L
        override fun onClick(v: View) {
            SystemClock.elapsedRealtime()
                    .takeIf { it - lastClickTime > debounceTime }
                    ?.run {
                        this@setClickListenerWithDebounce.onClick(v)
                        lastClickTime = this
                    }
        }
    }
    views.forEach { view -> view.setOnClickListener(clickListener) }
}

fun View.showAnimated(@AnimRes animationRes: Int = R.anim.show_in) {
    show()
    startAnimation(AnimationUtils.loadAnimation(context, animationRes))
}

fun View.hideAnimated(@AnimRes animationRes: Int = R.anim.show_out) {
    AnimationUtils.loadAnimation(context, animationRes).let {
        startAnimation(it)
        it.setAnimationListener(SimpleAnimationListener { hide() })
    }
}

fun View.expand(animationDuration: Long = DEFAULT_ANIMATION_DURATION) {
    val matchParentMeasureSpec = makeMeasureSpec((parent as View).width, View.MeasureSpec.EXACTLY)
    val wrapContentMeasureSpec = makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
    measure(matchParentMeasureSpec, wrapContentMeasureSpec)
    val targetHeight = measuredHeight
    layoutParams.height = 1
    show()
    val animation = object : Animation() {
        override fun applyTransformation(interpolatedTime: Float, t: Transformation?) {
            layoutParams.height = if (interpolatedTime == 1f) {
                ViewGroup.LayoutParams.WRAP_CONTENT
            } else {
                (targetHeight * interpolatedTime).toInt()
            }
            requestLayout()
        }

        override fun willChangeBounds() = true
    }
    animation.duration = animationDuration
    startAnimation(animation)
}

fun View.collapse(animationDuration: Long = DEFAULT_ANIMATION_DURATION) {
    val initialHeight = measuredHeight
    val animation = object : Animation() {

        override fun applyTransformation(interpolatedTime: Float, t: Transformation?) {
            if (interpolatedTime == 1f) {
                hide()
            } else {
                layoutParams.height = initialHeight - (initialHeight * interpolatedTime).toInt()
                requestLayout()
            }
        }

        override fun willChangeBounds() = true
    }
    animation.duration = animationDuration
    startAnimation(animation)
}

fun View.setAdditionalViewMargins(left: Int? = null,
                                  top: Int? = null,
                                  right: Int? = null,
                                  bottom: Int? = null) {
    (layoutParams as? ViewGroup.MarginLayoutParams)?.apply {
        setMargins(left?.plus(leftMargin) ?: leftMargin,
                top?.plus(topMargin) ?: topMargin,
                right?.plus(rightMargin) ?: rightMargin,
                bottom?.plus(bottomMargin) ?: bottomMargin)
    }?.let { params ->
        layoutParams = params
    }
}

/**
 * Call [View.requestApplyInsets] in a safe away. If we're attached it calls it straight-away.
 * If not it sets an [View.OnAttachStateChangeListener] and waits to be attached before calling
 * [View.requestApplyInsets].
 */
fun View.requestApplyInsetsWhenAttached() {
    if (isAttachedToWindow) {
        requestApplyInsets()
    } else {
        addOnAttachStateChangeListener(object : View.OnAttachStateChangeListener {
            override fun onViewAttachedToWindow(v: View) {
                v.requestApplyInsets()
            }

            override fun onViewDetachedFromWindow(v: View) = Unit
        })
    }
}

fun View.doOnApplyWindowInsets(f: (View, WindowInsetsCompat, ViewPaddingState) -> Unit) {
    // Create a snapshot of the view's padding state
    val paddingState = createPaddingStateForView(this)
    ViewCompat.setOnApplyWindowInsetsListener(this) { v, insets ->
        f(v, insets, paddingState)
        insets
    }
    requestApplyInsetsWhenAttached()
}

private fun createPaddingStateForView(view: View) = ViewPaddingState(view.paddingLeft,
        view.paddingTop, view.paddingRight, view.paddingBottom, view.paddingStart, view.paddingEnd)

data class ViewPaddingState(
        val left: Int,
        val top: Int,
        val right: Int,
        val bottom: Int,
        val start: Int,
        val end: Int
)
