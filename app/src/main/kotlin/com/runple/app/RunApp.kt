package com.runple.app

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.multidex.MultiDex
import com.cleveroad.bootstrap.kotlin_core.utils.Logger
import com.facebook.stetho.Stetho
import com.runple.app.preferences.PreferenceModule
import com.runple.app.utils.LocaleContextWrapper
import com.securepreferences.SecurePreferences
import java.util.*


class RunApp : Application() {

    companion object {
        lateinit var instance: RunApp
            private set
        lateinit var prefs: SharedPreferences
            private set
    }

    override fun attachBaseContext(base: Context) {
        prefs = if (BuildConfig.DEBUG) {
            getSharedPreferences(base)
        } else {
            getSecurePreferences(base)
        }
        val locale = Locale(PreferenceModule.lang ?: Locale.getDefault().language)
        super.attachBaseContext(LocaleContextWrapper.wrap(base, locale))

        MultiDex.install(base)
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        Logger.isDebugMode = BuildConfig.DEBUG
        if (BuildConfig.DEBUG) Stetho.initializeWithDefaults(this)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) setupChannels()
    }

    fun onLogout() {
        // TODO Starter: need implemented logout
        PreferenceModule.clearPrefs()
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private fun setupChannels() {
        // TODO Starter createNotificationChannel
    }


    private fun getSharedPreferences(base: Context) = base
            .getSharedPreferences(BuildConfig.SECURE_PREF_NAME, Context.MODE_PRIVATE)

    private fun getSecurePreferences(base: Context) = SecurePreferences(base,
            BuildConfig.SECURE_PREF_PASSWORD,
            BuildConfig.SECURE_PREF_NAME)
}
