package com.runple.app.database

import com.runple.app.BuildConfig

const val PROFILE_TABLE = "Profile"

//common
const val DB_VERSION = BuildConfig.DB_VERSION
