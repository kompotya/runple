package com.runple.app.database.dao

import androidx.room.Dao
import androidx.room.Query
import com.runple.app.database.PROFILE_TABLE
import com.runple.app.database.tables.UserDb
import io.reactivex.Single

@Dao
interface UserDao : BaseDao<UserDb> {

    @Query("DELETE FROM $PROFILE_TABLE")
    fun deleteAll()

    @Query("SELECT * FROM $PROFILE_TABLE LIMIT 1")
    fun getUser(): Single<UserDb>
}
