package com.runple.app.database.repositories

import com.runple.app.database.DatabaseCreator
import com.runple.app.database.converters.UserDBConverter
import com.runple.app.database.tables.UserDb
import com.runple.app.models.User
import io.reactivex.Single

interface UserRepository {

    fun saveUser(user: User): Single<User>

    fun deleteAll()

    fun getUser(): Single<User>
}

class UserRepositoryImpl : BaseRepository<UserDb>(), UserRepository {

    private val converter = UserDBConverter()

    private val dao = DatabaseCreator.database.userDao()

    override fun deleteAll() = dao.deleteAll()

    override fun saveUser(user: User): Single<User> =
            Single.just(user)
                    .compose(converter.single.inToOut())
                    .map {
                        dao.apply {
                            deleteAll()
                            insert(it)
                        }
                    }
                    .map { user }

    override fun getUser(): Single<User> =
            dao.getUser()
                    .compose(converter.single.outToIn())
}
