package com.runple.app.database.tables

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.runple.app.database.PROFILE_TABLE

@Entity(tableName = PROFILE_TABLE)
data class UserDb(
        @PrimaryKey var id: Long? = null,
        var email: String? = null,
        var phone: String? = null
)
