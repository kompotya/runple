package com.runple.app.database

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.room.Room
import com.cleveroad.bootstrap.kotlin_core.utils.completeToMain
import com.cleveroad.bootstrap.kotlin_core.utils.printLog
import com.runple.app.BuildConfig.DB_NAME
import io.reactivex.Completable
import java.util.concurrent.atomic.AtomicBoolean

/**
 * Creates the [AppDatabase] asynchronously, exposing a LiveData object to notify of creation.
 */
object DatabaseCreator {

    private val isDatabaseCreated = MutableLiveData<Boolean>()
    lateinit var database: RunpleDB
    private val initializing = AtomicBoolean(true)

    @SuppressWarnings("CheckResult")
    fun createDb(context: Context) {
        if (initializing.compareAndSet(true, false).not()) {
            return
        }

        isDatabaseCreated.value = false

        Completable.fromAction {
            database = Room.databaseBuilder(context, RunpleDB::class.java, DB_NAME).build()
        }
                .compose { completeToMain(it) }
                .subscribe({ isDatabaseCreated.value = true }, { it.printLog() })
    }
}
