package com.runple.app.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.runple.app.database.converters.type_converter.DateTypeConverter
import com.runple.app.database.dao.UserDao
import com.runple.app.database.tables.UserDb

@Database(entities = [UserDb::class], version = DB_VERSION)
@TypeConverters(value = [DateTypeConverter::class])
abstract class RunpleDB : RoomDatabase() {

    abstract fun userDao(): UserDao

}
