package com.runple.app.database

import android.annotation.SuppressLint
import com.cleveroad.bootstrap.kotlin_core.utils.printLog
import com.cleveroad.bootstrap.kotlin_ext.workAsync
import com.runple.app.database.repositories.UserRepositoryImpl
import io.reactivex.Single

object DatabaseModuleInjector {

    private var profileRepository: UserRepositoryImpl? = null

    fun getProfileRepository(): UserRepositoryImpl =
            profileRepository
                    ?: UserRepositoryImpl()
                            .apply { profileRepository = this }

    @SuppressLint("CheckResult")
    fun clearDatabase() {
        Single.fromCallable {
            getProfileRepository().deleteAll()
        }
                .workAsync()
                .subscribe({ it.printLog() }, { it.printLog() })
    }
}
