package com.runple.app.database.converters.type_converter

import androidx.room.TypeConverter
import org.joda.time.DateTime

class DateTypeConverter {

    @TypeConverter
    fun fromTimestamp(value: Long?) = value?.let { DateTime(it) }

    @TypeConverter
    fun dateToTimestamp(date: DateTime?) = date?.let { date.millis }
}
