package com.runple.app.database.converters

import com.runple.app.database.tables.UserDb
import com.runple.app.models.User
import com.runple.app.models.UserModel
import com.runple.app.models.converters.BaseConverter

class UserDBConverter : BaseConverter<User, UserDb>() {

    override fun processConvertInToOut(inObject: User?) = inObject?.run {
        UserDb(id, email, phone)
    }

    override fun processConvertOutToIn(outObject: UserDb?) = outObject?.run {
        UserModel(id, email, phone)
    }
}
