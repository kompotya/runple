package com.runple.app.ui.screens.auth.restore_password

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.cleveroad.bootstrap.kotlin_ext.applyIf
import com.cleveroad.bootstrap.kotlin_validators.ValidatorsFactory
import com.runple.app.providers.ProviderInjector
import com.runple.app.ui.base.BaseVM
import com.runple.app.utils.validation.ValidationField
import com.runple.app.utils.validation.ValidationResponseWrapper

class RestorePasswordVM(application: Application) : BaseVM(application) {

    val validationLD = MutableLiveData<ValidationResponseWrapper>()
    val restorePasswordLD = MutableLiveData<Unit>()

    private val emailValidator = ValidatorsFactory.getEmailValidator(application)

    private val accountProvider by lazy { ProviderInjector.getAccountProvider() }

    fun restorePassword(email: String) = applyIf(validateEmail(email)) { restore(email) }

    private fun validateEmail(email: String): Boolean =
            emailValidator.validate(email).run {
                validationLD.value = ValidationResponseWrapper(this, ValidationField.EMAIL)
                isValid
            }

    private fun restore(email: String) {
        accountProvider.restorePassword(email)
                .doAsync(restorePasswordLD)
    }
}