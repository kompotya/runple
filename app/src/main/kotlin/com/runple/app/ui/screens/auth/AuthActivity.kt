package com.runple.app.ui.screens.auth

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.runple.app.R
import com.runple.app.ui.base.BaseActivity
import com.runple.app.ui.screens.auth.confirm_password.ConfirmPasswordFragment
import com.runple.app.ui.screens.auth.restore_password.RestorePasswordFragment
import com.runple.app.ui.screens.auth.restore_password.RestorePasswordFragmentCallback
import com.runple.app.ui.screens.auth.sign_in.SignInCallback
import com.runple.app.ui.screens.auth.sign_in.SignInFragment
import com.runple.app.ui.screens.auth.sign_up.SignUpCallback
import com.runple.app.ui.screens.auth.sign_up.SignUpFragment
import com.runple.app.ui.screens.info.InfoFragment
import com.runple.app.ui.screens.info.TypeInfo

class AuthActivity : BaseActivity<AuthVM>(),
        SignUpCallback,
        SignInCallback,
        RestorePasswordFragmentCallback {

    companion object {

        fun start(context: Context?) {
            context?.apply { startActivity(getIntent(this)) }
        }

        fun getIntent(context: Context?) = Intent(context, AuthActivity::class.java)
    }

    override val viewModelClass = AuthVM::class.java

    override val containerId = R.id.flContainer

    override val layoutId = R.layout.activity_auth

    override fun hasProgressBar() = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        savedInstanceState ?: openSignUp()
    }

    override fun openSignIn() = replaceFragment(SignInFragment.newInstance(), false)

    override fun openSignUp() = replaceFragment(SignUpFragment.newInstance(), false)

    override fun openInfoScreen(typeInfo: TypeInfo) = replaceFragment(InfoFragment.newInstance(typeInfo))

    override fun openRestorePassword() = replaceFragment(RestorePasswordFragment.newInstance())

    override fun openConfirmPassword() = replaceFragment(ConfirmPasswordFragment.newInstance(), false)
}
