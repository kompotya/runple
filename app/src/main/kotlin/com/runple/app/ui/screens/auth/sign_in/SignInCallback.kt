package com.runple.app.ui.screens.auth.sign_in

interface SignInCallback {

    fun openSignUp()

    fun openRestorePassword()
}