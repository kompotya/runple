package com.runple.app.ui.screens.auth.confirm_password

interface ConfirmPasswordFragmentCallback {

    fun openSignIn()
}
