package com.runple.app.ui.screens.info

import android.annotation.TargetApi
import android.os.Build
import android.os.Bundle
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import com.cleveroad.bootstrap.kotlin_core.ui.NO_TITLE
import com.cleveroad.bootstrap.kotlin_ext.applyIf
import com.runple.app.R
import com.runple.app.ui.base.BaseFragment
import com.runple.app.ui.base.FragmentArgumentDelegate
import kotlinx.android.synthetic.main.fragment_info.*

class InfoFragment : BaseFragment<InfoViewModel>() {
    companion object {
        private const val FULLY_LOADED = 100

        fun newInstance(type: TypeInfo) = InfoFragment().apply { this.type = type }
    }

    override val viewModelClass = InfoViewModel::class.java
    override val layoutId = R.layout.fragment_info

    override fun getScreenTitle() = when (type) {
        TypeInfo.TERMS_OF_USE -> R.string.title_terms_of_use
        TypeInfo.PRIVACY_POLICY -> R.string.title_privacy_policy
        else -> NO_TITLE
    }

    override fun hasToolbar() = true
    override fun getToolbarId() = R.id.toolbar

    private val chromeClient = object : WebChromeClient() {
        override fun onProgressChanged(view: WebView?, newProgress: Int) {
            if (newProgress == FULLY_LOADED) hideProgress()
            super.onProgressChanged(view, newProgress)
        }
    }

    private val viewClient = object : WebViewClient() {

        @SuppressWarnings("deprecation")
        override fun shouldOverrideUrlLoading(view: WebView, url: String) = handleUri(view, url)

        @TargetApi(Build.VERSION_CODES.N)
        override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest) =
                handleUri(view, request.url.toString())

        private fun handleUri(view: WebView, url: String) =
                run { view.loadUrl(url) }.run { true }
    }

    private var type by FragmentArgumentDelegate<TypeInfo>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initWebView()
    }

    override fun handleNavigation() {
        wvTermsConditions.run {
            hideProgress()
            if (canGoBack()) goBack() else super.handleNavigation()
        }
    }

    override fun onBackPressed() = wvTermsConditions.run {
        hideProgress()
        applyIf(canGoBack()) { goBack() }
    }

    override fun onPause() {
        hideProgress()
        super.onPause()
    }

    private fun initWebView() = wvTermsConditions.run {
        settings.javaScriptEnabled = true
        settings.domStorageEnabled = true
        webChromeClient = chromeClient
        webViewClient = viewClient
        clearHistory()
        clearFormData()
        clearCache(true)
        //TODO Starter: change url
        loadUrl("https://www.google.com/")
    }
}