package com.runple.app.ui.screens.auth.sign_up

import com.runple.app.ui.screens.info.TypeInfo

interface SignUpCallback {

    fun openSignIn()

    fun openInfoScreen(typeInfo: TypeInfo)
}