package com.runple.app.ui.screens.info

enum class TypeInfo { TERMS_OF_USE, PRIVACY_POLICY }