package com.runple.app.ui.base

import android.content.Context
import android.content.Intent
import androidx.annotation.CallSuper
import com.cleveroad.bootstrap.kotlin_core.ui.BaseLifecycleActivity
import com.cleveroad.bootstrap.kotlin_core.ui.BaseLifecycleViewModel
import com.cleveroad.bootstrap.kotlin_core.ui.BlockedCallback
import com.google.android.material.snackbar.Snackbar
import com.runple.app.R
import com.runple.app.preferences.PreferenceModule
import com.runple.app.ui.base.dialog.DialogFragmentCallback
import com.runple.app.utils.LocaleContextWrapper
import java.util.*

abstract class BaseActivity<T : BaseLifecycleViewModel> : BaseLifecycleActivity<T>(),
        BlockedCallback, DialogFragmentCallback {

    override fun getProgressBarId() = R.id.progressView

    override fun getSnackBarDuration() = Snackbar.LENGTH_SHORT

    override fun onBlocked() = Unit

    /**
     * Add annotation in case if base implementation will be implemented
     * @see CallSuper
     */
    override fun observeLiveData() = Unit

    override fun attachBaseContext(base: Context) {
        val locale = Locale(PreferenceModule.lang ?: Locale.getDefault().language)
        super.attachBaseContext(LocaleContextWrapper.wrap(base, locale))
    }

    override fun onDialogResult(requestCode: Int, resultCode: Int, data: Intent) {
        val fragment = supportFragmentManager.findFragmentById(containerId)
        if (fragment is DialogFragmentCallback) fragment.onDialogResult(requestCode, resultCode, data)
    }
}