package com.runple.app.ui.screens.auth.sign_up

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.cleveroad.bootstrap.kotlin_core.ui.NO_TITLE
import com.cleveroad.bootstrap.kotlin_core.ui.NO_TOOLBAR
import com.cleveroad.bootstrap.kotlin_core.utils.misc.bindInterfaceOrThrow
import com.cleveroad.bootstrap.kotlin_ext.setClickListeners
import com.runple.app.R
import com.runple.app.extensions.*
import com.runple.app.models.User
import com.runple.app.ui.base.BaseFragment
import com.runple.app.ui.listeners.HideErrorTextWatcher
import com.runple.app.ui.screens.info.TypeInfo
import com.runple.app.utils.validation.ValidationField.*
import com.runple.app.utils.validation.ValidationResponseWrapper
import kotlinx.android.synthetic.main.fragment_sign_up.*

class SignUpFragment : BaseFragment<SignUpVM>(),
        View.OnClickListener {

    companion object {
        fun newInstance() = SignUpFragment().apply {
            arguments = Bundle()
        }
    }

    override val layoutId = R.layout.fragment_sign_up

    override val viewModelClass = SignUpVM::class.java

    private var callback: SignUpCallback? = null

    private val validationObserver = Observer<ValidationResponseWrapper> {
        when (it.field) {
            FIRST_NAME -> tilSignUpFirstName
            LAST_NAME -> tilSignUpLastName
            EMAIL -> tilSignUpEmail
            PASSWORD -> tilSignUpPassword
            CONFIRM_PASSWORD -> tilSignUpConfPassword
        }?.showTextInputError(it.response)
    }

    private val registrationObserver = Observer<Unit> {
        showSnackBar("Signed up")
    }

    override fun getScreenTitle() = NO_TITLE

    override fun getToolbarId() = NO_TOOLBAR

    override fun hasToolbar() = false

    override fun hasVersions() = true

    override fun observeLiveData() {
        viewModel.run {
            validationLD.safeObserve(this@SignUpFragment, validationObserver)
            registrationLD.safeObserve(this@SignUpFragment, registrationObserver)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        callback = bindInterfaceOrThrow<SignUpCallback>(context)
    }

    override fun onDetach() {
        callback = null
        super.onDetach()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setClickListeners(tvSignIn, bSignUp)
        etSignUpEmail.addTextWatcher(HideErrorTextWatcher(tilSignUpEmail))
        etSignUpConfPassword.addTextWatcher(HideErrorTextWatcher(tilSignUpConfPassword))
        etSignUpFirstName.addTextWatcher(HideErrorTextWatcher(tilSignUpFirstName))
        etSignUpLastName.addTextWatcher(HideErrorTextWatcher(tilSignUpLastName))

        val clickableText = listOf(
                TypeInfo.TERMS_OF_USE to getString(R.string.title_terms_of_use),
                TypeInfo.PRIVACY_POLICY to getString(R.string.title_privacy_policy)
        )
        tvTermsOfUses.addClickableText(getString(R.string.i_accept_the),
                getString(R.string.and), clickableText) { callback?.openInfoScreen(it) }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.tvSignIn -> callback?.openSignIn()
            R.id.bSignUp -> viewModel.signUp(etSignUpFirstName.text(),
                    etSignUpLastName.text(),
                    etSignUpEmail.text(),
                    etSignUpPassword.text(),
                    etSignUpConfPassword.text())
        }
    }
}
