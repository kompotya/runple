package com.runple.app.ui.screens.auth.confirm_password

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.cleveroad.bootstrap.kotlin_core.ui.NO_TITLE
import com.cleveroad.bootstrap.kotlin_core.ui.NO_TOOLBAR
import com.cleveroad.bootstrap.kotlin_core.utils.misc.bindInterfaceOrThrow
import com.cleveroad.bootstrap.kotlin_ext.getTrimStringText
import com.cleveroad.bootstrap.kotlin_ext.setClickListeners
import com.runple.app.R
import com.runple.app.extensions.safeSingleObserve
import com.runple.app.extensions.showTextInputError
import com.runple.app.ui.base.BaseFragment
import com.runple.app.ui.listeners.HideErrorTextWatcher
import com.runple.app.utils.validation.ValidationField
import com.runple.app.utils.validation.ValidationResponseWrapper
import kotlinx.android.synthetic.main.fragment_confirm_password.*

class ConfirmPasswordFragment : BaseFragment<ConfirmPasswordVM>(), View.OnClickListener {

    companion object {
        fun newInstance() = ConfirmPasswordFragment().apply { arguments = Bundle() }
    }

    override val viewModelClass = ConfirmPasswordVM::class.java
    override val layoutId = R.layout.fragment_confirm_password

    private var callback: ConfirmPasswordFragmentCallback? = null

    private val validationObserver = Observer<ValidationResponseWrapper> {
        when (it.field) {
            ValidationField.PASSWORD -> tilNewPassword
            ValidationField.CONFIRM_PASSWORD -> tilConfirmPass
            else -> null
        }?.showTextInputError(it.response)
    }

    private val confirmPassObserver = Observer<Unit> { onConfirmPasswordSuccess() }

    override fun getScreenTitle() = NO_TITLE
    override fun hasToolbar() = false
    override fun getToolbarId() = NO_TOOLBAR

    override fun onAttach(context: Context) {
        super.onAttach(context)
        callback = bindInterfaceOrThrow<ConfirmPasswordFragmentCallback>(context, parentFragment)
    }

    override fun onDetach() {
        callback = null
        super.onDetach()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        etNewPassword.addTextWatcher(HideErrorTextWatcher(tilNewPassword))
        etConfirmPass.addTextWatcher(HideErrorTextWatcher(tilConfirmPass))
        setClickListeners(bConfirmPass)
    }

    override fun observeLiveData() {
        viewModel.run {
            validationLD.safeSingleObserve(this@ConfirmPasswordFragment, validationObserver)
            confirmPasswordLD.safeSingleObserve(this@ConfirmPasswordFragment, confirmPassObserver)
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.bConfirmPass -> viewModel.confirmPassword(etNewPassword.getTrimStringText(),
                    etConfirmPass.getTrimStringText())
        }
    }

    private fun onConfirmPasswordSuccess() {
        showAlert(getString(R.string.confirm_new_password_message),
                getString(R.string.confirm_password),
                false,
                positiveFun = { callback?.openSignIn() })
    }
}
