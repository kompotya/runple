package com.runple.app.ui.screens.auth.sign_in

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.cleveroad.bootstrap.kotlin_core.ui.NO_TITLE
import com.cleveroad.bootstrap.kotlin_core.ui.NO_TOOLBAR
import com.cleveroad.bootstrap.kotlin_core.utils.misc.bindInterfaceOrThrow
import com.cleveroad.bootstrap.kotlin_ext.setClickListeners
import com.runple.app.R
import com.runple.app.extensions.safeObserve
import com.runple.app.extensions.showTextInputError
import com.runple.app.extensions.text
import com.runple.app.ui.base.BaseFragment
import com.runple.app.ui.listeners.HideErrorTextWatcher
import com.runple.app.utils.validation.ValidationField
import com.runple.app.utils.validation.ValidationResponseWrapper
import kotlinx.android.synthetic.main.fragment_sign_in.*

class SignInFragment : BaseFragment<SignInVM>(),
        View.OnClickListener {

    companion object {
        fun newInstance() = SignInFragment()
    }

    override val layoutId: Int = R.layout.fragment_sign_in

    override val viewModelClass = SignInVM::class.java

    private var callback: SignInCallback? = null

    private val validationObserver = Observer<ValidationResponseWrapper> {
        when (it.field) {
            ValidationField.EMAIL -> tilSignInEmail
            ValidationField.PASSWORD -> tilSignInPassword
            else -> null
        }?.showTextInputError(it.response)
    }

    private val authorizationObserver = Observer<Unit> {
        showSnackBar("Signed in")
    }

    override fun getScreenTitle() = NO_TITLE

    override fun getToolbarId() = NO_TOOLBAR

    override fun hasToolbar() = false

    override fun observeLiveData() {
        super.observeLiveData()
        viewModel.run {
            validationLD.safeObserve(this@SignInFragment, validationObserver)
            authorizationLD.safeObserve(this@SignInFragment, authorizationObserver)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        callback = bindInterfaceOrThrow<SignInCallback>(context)
    }

    override fun onDetach() {
        callback = null
        super.onDetach()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setClickListeners(bSignIn, bSignUp, tvRestorePassword)
        etSignInEmail.addTextWatcher(HideErrorTextWatcher(tilSignInEmail))
        etSignInPassword.addTextWatcher(HideErrorTextWatcher(tilSignInPassword))
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.bSignIn -> viewModel.signIn(etSignInEmail.text(), etSignInPassword.text())
            R.id.bSignUp -> callback?.openSignUp()
            R.id.tvRestorePassword -> callback?.openRestorePassword()
        }
    }
}