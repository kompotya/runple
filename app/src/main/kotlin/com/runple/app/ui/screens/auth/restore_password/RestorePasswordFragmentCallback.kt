package com.runple.app.ui.screens.auth.restore_password

interface RestorePasswordFragmentCallback {

    fun openConfirmPassword()
}
