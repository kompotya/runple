package com.runple.app.ui.screens.auth.confirm_password

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.cleveroad.bootstrap.kotlin_ext.applyIf
import com.cleveroad.bootstrap.kotlin_validators.MatchPasswordValidator
import com.cleveroad.bootstrap.kotlin_validators.ValidatorsFactory
import com.runple.app.providers.ProviderInjector
import com.runple.app.ui.base.BaseVM
import com.runple.app.utils.validation.ValidationField
import com.runple.app.utils.validation.ValidationResponseWrapper

class ConfirmPasswordVM(application: Application) : BaseVM(application) {

    val validationLD = MutableLiveData<ValidationResponseWrapper>()
    val confirmPasswordLD = MutableLiveData<Unit>()

    private val matchPasswordsValidator = ValidatorsFactory.getMatchPasswordValidator(application)

    private val accountProvider by lazy { ProviderInjector.getAccountProvider() }

    fun confirmPassword(newPassword: String, confirmPassword: String) =
            applyIf(validateMatchPasswords(newPassword, confirmPassword)) {
                confirmPassword(newPassword)
            }

    private fun validateMatchPasswords(password: String, confirmPassword: String): Boolean =
            matchPasswordsValidator.validate(password, confirmPassword).run {
                validationLD.value = ValidationResponseWrapper(this,
                        when (invalidFieldNumber) {
                            MatchPasswordValidator.PASSWORD_ERROR_FIELD -> ValidationField.PASSWORD
                            else -> ValidationField.CONFIRM_PASSWORD
                        })
                isValid
            }

    private fun confirmPassword(password: String) =
            accountProvider.confirmPassword(password)
                    .doAsync(confirmPasswordLD)
}