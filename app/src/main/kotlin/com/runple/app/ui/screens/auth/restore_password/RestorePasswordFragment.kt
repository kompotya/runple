package com.runple.app.ui.screens.auth.restore_password

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.cleveroad.bootstrap.kotlin_core.ui.NO_TITLE
import com.cleveroad.bootstrap.kotlin_core.ui.NO_TOOLBAR
import com.cleveroad.bootstrap.kotlin_core.utils.misc.bindInterfaceOrThrow
import com.cleveroad.bootstrap.kotlin_ext.setClickListeners
import com.runple.app.R
import com.runple.app.extensions.safeSingleObserve
import com.runple.app.extensions.showTextInputError
import com.runple.app.extensions.text
import com.runple.app.ui.base.BaseFragment
import com.runple.app.ui.listeners.HideErrorTextWatcher
import com.runple.app.utils.validation.ValidationResponseWrapper
import kotlinx.android.synthetic.main.fragment_restore_password.*

class RestorePasswordFragment : BaseFragment<RestorePasswordVM>(), View.OnClickListener {

    companion object {
        fun newInstance() = RestorePasswordFragment().apply { arguments = Bundle() }
    }

    override val viewModelClass = RestorePasswordVM::class.java

    override val layoutId = R.layout.fragment_restore_password

    override fun getScreenTitle() = NO_TITLE
    override fun hasToolbar() = false
    override fun getToolbarId() = NO_TOOLBAR
    override fun hasVersions() = true

    private var callback: RestorePasswordFragmentCallback? = null

    private val validationObserver = Observer<ValidationResponseWrapper> {
        tilRestoreEmail.showTextInputError(it.response)
    }

    private val restoreObserver = Observer<Unit> { onRestorePasswordSuccessful() }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        callback = bindInterfaceOrThrow(context, parentFragment)
    }

    override fun onDetach() {
        callback = null
        super.onDetach()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        etRestoreEmail.addTextWatcher(HideErrorTextWatcher(tilRestoreEmail))
        setClickListeners(bRestorePass)
    }

    override fun observeLiveData() {
        viewModel.run {
            validationLD.safeSingleObserve(this@RestorePasswordFragment, validationObserver)
            restorePasswordLD.safeSingleObserve(this@RestorePasswordFragment, restoreObserver)
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.bRestorePass -> viewModel.restorePassword(etRestoreEmail.text())
        }
    }

    private fun onRestorePasswordSuccessful() {
        showAlert(getString(R.string.restore_password_message),
                getString(R.string.restore_password),
                false,
                positiveRes = R.string.ok,
                positiveFun = { callback?.openConfirmPassword() })
    }
}
