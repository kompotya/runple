package com.runple.app.ui.screens.splash

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.runple.app.R
import com.runple.app.ui.base.BaseActivity
import com.runple.app.ui.screens.auth.AuthActivity

class SplashActivity : BaseActivity<SplashVM>() {

    companion object {

        fun start(context: Context?) {
            context?.apply ctx@{ startActivity(getIntent(this@ctx)) }
        }

        fun getIntent(context: Context?) = Intent(context, SplashActivity::class.java)
    }

    override val viewModelClass = SplashVM::class.java
    override val containerId = R.id.flContainer
    override val layoutId = R.layout.activity_splash

    override fun hasProgressBar() = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        openAuthScreen()
    }

    private fun openAuthScreen() {
        AuthActivity.start(this)
        finish()
    }
}
