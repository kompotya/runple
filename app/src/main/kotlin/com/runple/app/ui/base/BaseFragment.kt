package com.runple.app.ui.base

import android.content.Intent
import android.graphics.Rect
import android.os.Bundle
import android.view.View
import android.view.ViewTreeObserver
import androidx.annotation.CallSuper
import com.cleveroad.bootstrap.kotlin_core.ui.BaseLifecycleFragment
import com.cleveroad.bootstrap.kotlin_core.ui.BaseLifecycleViewModel
import com.cleveroad.bootstrap.kotlin_core.utils.printLog
import com.cleveroad.bootstrap.kotlin_ext.workAsync
import com.runple.app.BuildConfig
import com.runple.app.R
import com.runple.app.ui.base.dialog.DialogFragmentCallback
import com.tbruyelle.rxpermissions2.RxPermissions
import io.reactivex.disposables.CompositeDisposable

abstract class BaseFragment<T : BaseLifecycleViewModel> : BaseLifecycleFragment<T>(),
        DialogFragmentCallback {

    companion object {
        private const val KEYBOARD_VISIBLE_THRESHOLD_DP = 300
    }


    private val permissionDisposable = CompositeDisposable()

    private val rxPermission by lazy { RxPermissions(this) }

    private val keyboardListener = ViewTreeObserver.OnGlobalLayoutListener {
        view?.run {
            val rect = Rect()
            getWindowVisibleDisplayFrame(rect)
            val heightChangView = rootView.height - (rect.bottom - rect.top)
            when {
                !blockKeyboardListener && heightChangView > KEYBOARD_VISIBLE_THRESHOLD_DP -> {
                    blockKeyboardListener = true
                    onKeyboardSwitch(true)
                }
                blockKeyboardListener && heightChangView <= KEYBOARD_VISIBLE_THRESHOLD_DP -> {
                    blockKeyboardListener = false
                    onKeyboardSwitch(false)
                }
            }
        }
    }

    private var blockKeyboardListener: Boolean = true

    override var endpoint = BuildConfig.ENDPOINT

    override var versionName = BuildConfig.VERSION_NAME

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (needKeyboardListener()) view.viewTreeObserver.addOnGlobalLayoutListener(keyboardListener)
    }

    /**
     * Add annotation in case if base implementation will be implemented
     * @see CallSuper
     */
    override fun observeLiveData() = Unit

    override fun onDestroyView() {
        permissionDisposable.clear()
        if (needKeyboardListener()) view?.viewTreeObserver?.removeOnGlobalLayoutListener(keyboardListener)
        super.onDestroyView()
    }

    override fun getVersionsLayoutId() = R.id.versionsContainer

    override fun getEndPointTextViewId() = R.id.tvEndpoint

    override fun getVersionsTextViewId() = R.id.tvVersion

    override fun isDebug() = BuildConfig.DEBUG

    /**
     * Display a warning when going to action back
     */
    override fun showBlockBackAlert() = Unit

    /**
     * Turn on/off keyboard listener
     */
    protected open fun needKeyboardListener() = false

    /**
     * This method is called when keyboard change state
     */
    protected open fun onKeyboardSwitch(isShow: Boolean) = Unit

    override fun onDialogResult(requestCode: Int, resultCode: Int, data: Intent) = Unit

    protected fun requestPermission(
            vararg permission: String,
            isDeniedCallback: () -> Unit = { },
            isGrantedCallback: () -> Unit = { }
    ) {
        rxPermission.request(*permission)
                ?.workAsync()
                ?.subscribe({ granted ->
                    if (granted) isGrantedCallback() else isDeniedCallback()
                }, {
                    it.printLog()
                })?.let { permissionDisposable.add(it) }
    }
}