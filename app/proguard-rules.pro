# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile
# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile
-dontwarn
-ignorewarnings

##---------------Begin: OKHTTP3  ----------
-dontwarn javax.annotation.**
-keepnames class okhttp3.internal.publicsuffix.PublicSuffixDatabase
-dontwarn org.codehaus.mojo.animal_sniffer.*
-dontwarn okhttp3.internal.platform.ConscryptPlatform
##---------------End: OKHTTP3  ----------
##---------------Begin: Okio  ----------
-dontwarn org.codehaus.mojo.animal_sniffer.*
##---------------End: Okio ----------
##---------------Begin: Retrofit2  ----------
-keepattributes Signature, InnerClasses, EnclosingMethod
-keepattributes RuntimeVisibleAnnotations, RuntimeVisibleParameterAnnotations
-keepclassmembers,allowshrinking,allowobfuscation interface * {
    @retrofit2.http.* <methods>;
}
-dontwarn org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement
-dontwarn javax.annotation.**
-dontwarn kotlin.Unit
-dontwarn retrofit2.KotlinExtensions
-if interface * { @retrofit2.http.* <methods>; }
-keep,allowobfuscation interface <1>
##---------------End: Retrofit2  ----------
##---------------Begin: Jackson  ----------
-keep @com.fasterxml.jackson.annotation.JsonIgnoreProperties class * { *; }
-keep public class com.fasterxml.jackson.annotation.JsonProperty
-keep class com.fasterxml.** { *; }
-keep class org.codehaus.** { *; }
-keepnames class com.fasterxml.jackson.** { *; }
-keepclassmembers public final enum com.fasterxml.jackson.annotation.JsonAutoDetect$Visibility {
    public static final com.fasterxml.jackson.annotation.JsonAutoDetect$Visibility *;
}
-keepclassmembers public final enum org.codehaus.jackson.annotate.JsonAutoDetect$Visibility {
    public static final org.codehaus.jackson.annotate.JsonAutoDetect$Visibility *; }
-keep class com.fasterxml.jackson.databind.ObjectMapper {
    public <methods>;
    protected <methods>;
}
-keep class com.fasterxml.jackson.databind.ObjectWriter {
    public ** writeValueAsString(**);
}
-dontwarn com.fasterxml.jackson.databind.**
-keepclassmembers class * {
     @com.fasterxml.jackson.annotation.JsonCreator *;
     @com.fasterxml.jackson.annotation.JsonProperty *;
}
-keepnames class * { *; }
-keep class kotlin.Metadata { *; }
##---------------End: Jackson  ----------
##---------------Begin: Stetho  ----------
-keep class com.facebook.stetho.** { *; }
-dontwarn com.facebook.stetho.**
##---------------End: Stetho  ----------
##---------------Begin: Glide  ----------
-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public class * extends com.bumptech.glide.module.AppGlideModule
-keep public enum com.bumptech.glide.load.ImageHeaderParser$** {
  **[] $VALUES;
  public *;
}
##---------------End: Glide  ----------
##---------------Begin: Fabric  ----------
-keepattributes *Annotation*
-keepattributes SourceFile,LineNumberTable
-keep public class * extends java.lang.Exception
##---------------End: Fabric  ----------
##---------------Begin: Secure Preference  ----------
-keep class com.tozny.crypto.android.AesCbcWithIntegrity$PrngFixes$* { *; }
##---------------End: Secure Preference  ----------
##---------------Begin: Request ----------
##TODO Starter add your requests here
##---------------End: Request  ----------
##---------------Begin: org json mappers  ----------
##TODO Starter add your mappers here
##---------------End: org json mappers  ----------
##---------------Begin: Beans ----------
##TODO Starter add your beans here
##---------------End: Beans ----------

